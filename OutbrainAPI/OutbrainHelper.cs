﻿using CsvHelper;
using Newtonsoft.Json.Linq;
using OutbrainAPI.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OutbrainAPI
{
    public class OutbrainHelper
    {
        private bool isRealTimePull;
        private string token;
        List<Marketers> marketers;

        #region Constructor
        public OutbrainHelper(bool isRealTimePull)
        {
            this.isRealTimePull = isRealTimePull;
        }
        #endregion

        public async Task SetToken(string username, string password)
        {

            var tokenHelper = new TokenSettingsHelper(username);
            this.token = tokenHelper.RetrieveAuthToken();

            if (this.token == null)
            {
                //Read the token from the config
                using (var client = new HttpClient())
                {
                    // Set credentials
                    var credentials = Encoding.ASCII.GetBytes(String.Format("{0}:{1}", username, password));

                    APIRateLimiter.ThrottleRequest();

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    // Get token
                    HttpResponseMessage response = await client.GetAsync("https://api.outbrain.com/amplify/v0.1/login");
                    string responseString = await response.Content.ReadAsStringAsync();

                    // Parse token
                    var jsonParsed = JObject.Parse(responseString);
                    this.token = jsonParsed.SelectToken("OB-TOKEN-V1").Value<string>();
                }

                tokenHelper.StoreAuthToken(this.token);
            }
        }

        public async Task GetMarketers()
        {
            using (var client = new HttpClient())
            {
                // Get marketers
                List<Marketers> marketers = new List<Marketers>();
                HttpRequestMessage mRequest = new HttpRequestMessage(HttpMethod.Get, "https://api.outbrain.com/amplify/v0.1/marketers");
                mRequest.Headers.Add("OB-TOKEN-V1", this.token);

                APIRateLimiter.ThrottleRequest();
                await client.SendAsync(mRequest).ContinueWith(task =>
                {
                    // Get marketers response
                    string responseString = task.Result.Content.ReadAsStringAsync().Result;

                    // Parse marketers
                    var jsonParsed = JObject.Parse(responseString);
                    var m = jsonParsed.SelectToken("marketers");
                    this.marketers = m.ToObject<List<Marketers>>();
                });
            }
        }


        public async Task GetCampaigns(bool includeArchive)
        {
            using (var client = new HttpClient())
            {
                // Get campaigns for each marketer
                foreach (var m in this.marketers)
                {
                    Console.WriteLine("{0}: {1}", m.Name, m.Id);
                    bool retry = true;
                    int retryCounter = 0;
                    while (retry)
                    {
                        try
                        {
                            HttpRequestMessage cRequest = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.outbrain.com/amplify/v0.1/marketers/{0}/campaigns?includeArchived={1}", m.Id, includeArchive ? "true" : "false"));
                            cRequest.Headers.Add("OB-TOKEN-V1", this.token);

                            APIRateLimiter.ThrottleRequest();
                            await client.SendAsync(cRequest).ContinueWith(task =>
                            {
                                // Get marketers response
                                string responseString = task.Result.Content.ReadAsStringAsync().Result;

                                // Parse campaigns
                                var jsonParsed = JObject.Parse(responseString);
                                var c = jsonParsed.SelectToken("campaigns");
                                m.Campaigns = c.ToObject<List<Campaign>>();

                                Console.WriteLine("{0} campaigns", m.Campaigns.Count);
                            });
                            retry = false;
                        }
                        catch (Exception e)
                        {
                            //Sleep for 30 seconds and retry
                            Thread.Sleep(30 * 1000);

                            //Retry 3 times
                            retryCounter++;
                            if (retryCounter > 3)
                            {
                                throw e;
                            }
                        }
                    }
                }
            }
        }

        // NOTE: This function will parse the PromotedLinks for each campaign.  However, it will only parse at max 100 Promoted Links (if a campaign has more than 100 promoted links).
        public async Task GetCampaignURLs()
        {
            using (var client = new HttpClient())
            {
                // Get campaigns for each marketer
                foreach (var marketer in this.marketers)
                {
                    foreach (var campaign in marketer.Campaigns)
                    {
                        campaign.PromotedLinks = new List<PromotedLink>();
                        while (campaign.PromotedLinks.Count == 0 || campaign.PromotedLinks.Count < campaign.TotalPromotedLinks)
                        {
                            HttpRequestMessage cRequest = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.outbrain.com/amplify/v0.1/campaigns/{0}/promotedLinks?limit=100&offset={1}", campaign.Id, campaign.PromotedLinks.Count));
                            cRequest.Headers.Add("OB-TOKEN-V1", this.token);

                            APIRateLimiter.ThrottleRequest();
                            await client.SendAsync(cRequest).ContinueWith(task =>
                            {
                                // Get marketers response
                                string responseString = task.Result.Content.ReadAsStringAsync().Result;

                                // Retrieve URL
                                var jsonParsed = JObject.Parse(responseString);
                                var pl = jsonParsed.SelectToken("promotedLinks");
                                campaign.PromotedLinks.AddRange(pl.ToObject<List<PromotedLink>>());
                                campaign.TotalPromotedLinks = jsonParsed.SelectToken("totalCount").ToObject<int>();
                            });
                            if (campaign.TotalPromotedLinks == 0)
                                break;
                        }
                    }
                }
            }
        }

        public async Task GetCampaignStats(DateTime fromDate, DateTime toDate)
        {
            // Don't need to deep dive into stats for realtime pulls (can just use LiveStatus.cs)
            if (this.isRealTimePull)
                return;

            // Get stats for each campaign
            foreach (var marketer in this.marketers)
            {
                foreach (var campaign in marketer.Campaigns)
                {
                    bool retry = true;
                    int retryCounter = 0;
                    while (retry)
                    {
                        try
                        {
                            using (var client = new HttpClient())
                            {
                                HttpRequestMessage cRequest = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.outbrain.com/amplify/v0.1/campaigns/{0}/performanceByDay/?from={1}&to={2}&limit=100&offset=0&sort=-date", campaign.Id, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd")));
                                cRequest.Headers.Add("OB-TOKEN-V1", this.token);

                                APIRateLimiter.ThrottleRequest();

                                await client.SendAsync(cRequest).ContinueWith(task =>
                                {
                                    // Get marketers response
                                    string responseString = task.Result.Content.ReadAsStringAsync().Result;

                                    // Parse campaigns
                                    var jsonParsed = JObject.Parse(responseString);
                                    campaign.PerformanceReport = jsonParsed.ToObject<Report_PerformanceCampaign>();

                                    Console.WriteLine("{0}: ${1}", campaign.Name, campaign.PerformanceReport.OverallMetrics.Cost);
                                });
                            }
                            retry = false;
                        }
                        catch (Exception e)
                        {
                            //Sleep for 30 seconds and retry
                            Thread.Sleep(30 * 1000);

                            //Retry 3 times
                            retryCounter++;
                            if (retryCounter > 3)
                            {
                                throw e;
                            }
                        }
                    }
                }
            }
        }


        public async Task GetPerformanceDetails(DateTime fromDate, DateTime toDate)
        {
            // Don't need to deep dive into stats for realtime pulls (can just use LiveStatus.cs)
            if (this.isRealTimePull)
                return;

            // Get stats for each campaign
            foreach (var marketer in this.marketers)
            {
                foreach (var campaign in marketer.Campaigns)
                {
                    if (campaign.PerformanceReport.OverallMetrics.Cost.ParseAs<decimal>(0) == 0)
                        continue;

                    using (var client = new HttpClient())
                    {
                        HttpRequestMessage cRequest = new HttpRequestMessage(HttpMethod.Get,
                            string.Format("https://api.outbrain.com/amplify/v0.1/campaigns/{0}/performanceByPromotedLink/?from={1}&to={2}&limit=1000&offset=0&sort=-ctr", campaign.Id, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd")));

                        cRequest.Headers.Add("OB-TOKEN-V1", this.token);

                        APIRateLimiter.ThrottleRequest();

                        await client.SendAsync(cRequest).ContinueWith(task =>
                        {
                            // Get marketers response
                            string responseString = task.Result.Content.ReadAsStringAsync().Result;

                            // Parse campaigns
                            var jsonParsed = JObject.Parse(responseString);
                            var response = jsonParsed.ToObject<PerformanceByPromotedLink>();
                            foreach(var responseItem in response.details)
                            {
                                var promotedLink = campaign.PromotedLinks.Where(o => o.Id == responseItem.id).FirstOrDefault();
                                if (promotedLink != null)
                                {
                                    promotedLink.HasSpend = true;
                                }
                            }
                        });
                    }
                    var archivedPromotedLinks = 0;
                    var skippedPromotedLinks = 0;
                    var queriedPromotedLinks = 0;
                    foreach (var promotedLink in campaign.PromotedLinks)
                    {
                        if (promotedLink.Archived)
                        {
                            archivedPromotedLinks++;
                            continue;
                        }
                        if (!promotedLink.HasSpend)
                        {
                            skippedPromotedLinks++;
                            continue;
                        }
                        queriedPromotedLinks++;

                        bool retry = true;
                        int retryCounter = 0;
                        while (retry)
                        {
                            try
                            {
                                using (var client = new HttpClient())
                                {
                                    HttpRequestMessage cRequest = new HttpRequestMessage(HttpMethod.Get,
                                        string.Format("https://api.outbrain.com/amplify/v0.1/promotedLinks/{0}/performanceByDay/?from={1}&to={2}&limit=100&offset=0&sort=-ctr", promotedLink.Id, fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd")));

                                    cRequest.Headers.Add("OB-TOKEN-V1", this.token);

                                    APIRateLimiter.ThrottleRequest();

                                    await client.SendAsync(cRequest).ContinueWith(task =>
                                    {
                                        // Get marketers response
                                        string responseString = task.Result.Content.ReadAsStringAsync().Result;

                                        // Parse campaigns
                                        var jsonParsed = JObject.Parse(responseString);
                                        promotedLink.PromotedLinkReport = jsonParsed.ToObject<PromotedLinksByDay>();

                                        //Console.WriteLine("{0}:{1}: ${2}", promotedLink.Url, campaign.Name, promotedLink.PromotedLinkReport.OverallMetrics.Cost);
                                    });
                                    retry = false;
                                }
                            }
                            catch (Exception e)
                            {
                                //Sleep for 30 seconds and retry
                                Thread.Sleep(30 * 1000);

                                //Retry 3 times
                                retryCounter++;
                                if (retryCounter > 3)
                                {
                                    throw e;
                                }
                            }
                        }
                    }

                    Console.WriteLine("Pulled Promoted Links for {0}: ${1}::: {4} Queried: {2} Archived,:{3} with no spend", campaign.Name, campaign.PromotedLinks.Where(o=>o.PromotedLinkReport != null).Sum(o=>o.PromotedLinkReport.OverallMetrics.Cost.ParseAs<decimal>()), archivedPromotedLinks, skippedPromotedLinks, queriedPromotedLinks);

                }
            }
        }

    public async Task LoadCampaignStats(DateTime fromDate, DateTime toDate)
    {
        var srData = Db.LoadSpendReportData(this.isRealTimePull, this.marketers, fromDate, toDate);

        //if (!isRealTimePull)
        //{
        //    var maxDate = srData.Select(s => s.Date).Max().Date;

        //    var analyticsHelper = new AnalyticsHelper();

        //    analyticsHelper.UpdateCostData("filthyrich.es", maxDate);
        //    analyticsHelper.UpdateCostData("travelversed.co", maxDate);
        //    analyticsHelper.UpdateCostData("travelversed.com", maxDate);

        //}

    }


    public async Task ExportCampaignStats()
    {
        List<CampaignSimpleTest> data = new List<CampaignSimpleTest>();
        foreach (var marketer in this.marketers)
        {
            foreach (var campaign in marketer.Campaigns)
            {
                if (this.isRealTimePull)
                {
                    var entry = new CampaignSimpleTest()
                    {
                        Date = DateTime.Today.ToShortDateString(),
                        Marketer = marketer.Name,
                        Campaign = campaign.Name,
                        PromotedLinkFirst = campaign.PromotedLinks.Count > 0 ? campaign.PromotedLinks[0].Url : "NA",
                        Cost = campaign.LiveStatus.AmountSpent
                    };
                    data.Add(entry);
                }
                else
                {
                    foreach (var stat in campaign.PerformanceReport.Details)
                    {
                        var entry = new CampaignSimpleTest()
                        {
                            Date = stat.Date,
                            Marketer = marketer.Name,
                            Campaign = campaign.Name,
                            PromotedLinkFirst = campaign.PromotedLinks.Count > 0 ? campaign.PromotedLinks[0].Url : "NA",
                            Impressions = stat.Impressions,
                            Clicks = stat.Clicks,
                            Cost = stat.Cost
                        };

                        // Parse domain from url
                        var domain = HttpHelper.RootDomain(campaign.PromotedLinks.Count > 0 ? campaign.PromotedLinks.First().Url : string.Empty);
                        // Concat marketer/account name with domain for SpendAccount.Username lookup
                        var sa = Db.GetSpendAccount(string.Format("{0}_{1}", marketer.Name, domain));

                        entry.SpendAccountId = sa.Id;

                        data.Add(entry);
                    }
                }
            }
        }

        var file = @"C:\Projects\70degrees\OutbrainAPI\Files\test.csv";
        Console.WriteLine("Exporting campaign stats to {0}", file);
        using (var textWriter = File.CreateText(file))
        {
            var csv = new CsvWriter(textWriter);
            csv.WriteRecords(data);
        }


    }

}
}
