﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OutbrainAPI
{
    public class APIRateLimiter
    {
        private static long MaxRequestPerSecond = 30;
        private static long LastRequest = 0;
        public static void UpdateMaxRequestPerSecond(long rps)
        {
            MaxRequestPerSecond = rps;
        }
        public static void ThrottleRequest()
        {
            var minimumRequestTime = 1000 / MaxRequestPerSecond;

            var timeSinceLastRequest = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - LastRequest;
            if (timeSinceLastRequest < minimumRequestTime)
               Thread.Sleep((int)(minimumRequestTime - timeSinceLastRequest));

            LastRequest = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond; 


        }
    }
}
