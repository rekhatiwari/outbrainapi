﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbrainAPI
{
    public class HttpHelper
    {

        /// <summary>
        /// Gets the root domain for a given host
        /// </summary>
        /// <param name="host">string url</param>
        /// <returns>root domain as string</returns>
        public static string RootDomain(string host)
        {
            if (!string.IsNullOrEmpty(host))
            {
                if (!host.Contains("://"))
                    host = "http://" + host;

                string domain = new Uri(host).Host;
                domain = domain.Replace("www.", "");    // remove www. from domain if exists

                // Strip sub-domain
                if (domain.Contains("co.uk"))
                    return domain;

                string[] domainSplit = domain.Split('.');

                if (domainSplit.Length > 2)
                {
                    domain = string.Format("{0}.{1}", domainSplit[domainSplit.Length - 2], domainSplit[domainSplit.Length - 1]);
                }

                return domain;
            }
            return string.Empty;
        }

    }
}
