﻿namespace OutbrainAPI.Entities
{
    public class CampaignDailyDetail
    {

        public string Date { get; set; }
        public string Cpa { get; set; }
        public string eCpc { get; set; }
        public string Ctr { get; set; }
        public string Cost { get; set; }
        public string Conversions { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }

    }
}
