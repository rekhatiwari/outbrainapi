﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbrainAPI.Entities
{
    public class PromotedLinksByDay
    {
        public string CurrencyName { get; set; }
        public int TotalDataCount { get; set; }
        public OverallMetrics OverallMetrics { get; set; }

        public List<OverallMetrics> details { get; set; }

        public string AggregatedBy { get; set; }

        public string CampaignId { get; set; }


    }
}
