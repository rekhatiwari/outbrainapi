﻿using System.Collections.Generic;

namespace OutbrainAPI.Entities
{
    public class Marketers
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string Enabled { get; set; }
        public string CreationTime { get; set; }
        public string LastModified { get; set; }

        public List<Campaign> Campaigns { get; set; }

    }
}
