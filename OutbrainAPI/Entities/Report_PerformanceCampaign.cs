﻿using System.Collections.Generic;

namespace OutbrainAPI.Entities
{
    public class Report_PerformanceCampaign
    {

        public OverallMetrics OverallMetrics { get; set; }
        public string CurrencyName { get; set; }
        public string TotalDataCount { get; set; }
        public string AggregatedBy { get; set; }
        public List<CampaignDailyDetail> Details { get; set; }

    }
}
