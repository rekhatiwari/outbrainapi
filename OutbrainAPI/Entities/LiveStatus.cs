﻿namespace OutbrainAPI.Entities
{
    public class LiveStatus
    {

        public string CampaignOnAir { get; set; }
        public string OnAirReason { get; set; }
        public string AmountSpent { get; set; }

    }
}
