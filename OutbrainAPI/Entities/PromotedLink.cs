﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbrainAPI.Entities
{
    public class PromotedLink
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string CreationTime { get; set; }
        public string LastModified { get; set; }
        public string Url { get; set; }
        public string SiteName { get; set; }
        public string SectionName { get; set; }
        public string CampaignId { get; set; }
        public bool Archived { get; set; }
        public string DocumentLanguage { get; set; }

        public PromotedLinksByDay PromotedLinkReport { get; set; }
        public bool HasSpend { get; set; }
    }
}

