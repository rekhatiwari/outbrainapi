﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbrainAPI.Entities
{
    public class PerformanceByPromotedLink
    {
        public OverallMetrics overallMetrics;
        public string currencyName;
        public int totalDataCount;
        public string aggregatedBy;
        public List<PromotedLinkPerformance> details;
    }

    public class PromotedLinkPerformance
    {
        public string id;
        public string text;
        public string url;
        public decimal cost;
        public decimal cpa;
        public decimal eCPC;
        public decimal ctr;
        public int conversions;
        public int clicks;
        public int impressions;
    }

}
