﻿using System.Collections.Generic;

namespace OutbrainAPI.Entities
{
    public class Campaign
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string MarketerId { get; set; }
        public string OnAirReason { get; set; }
        public string Enabled { get; set; }
        public string LastModified { get; set; }
        public string CreationTime { get; set; }
        public string AutoArchived { get; set; }
        public string Currency { get; set; }
        public string Cpc { get; set; }
        public string MinimumCpc { get; set; }
        public string AmountSpent { get; set; }
        public Targeting Targeting { get; set; }
        public List<string> Feeds { get; set; }
        public Budget Budget { get; set; }
        public string SuffixTrackingCode { get; set; }
        public LiveStatus LiveStatus { get; set; }

        public Report_PerformanceCampaign PerformanceReport { get; set; }

        public List<PromotedLink> PromotedLinks { get; set; }
        public int TotalPromotedLinks { get; set; }

    }

}
