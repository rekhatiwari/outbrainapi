﻿namespace OutbrainAPI.Entities
{
    public class Budget
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string Shared { get; set; }
        public string Amount { get; set; }
        public string Currency { get; set; }
        public string AmountRemaining { get; set; }
        public string AmountSpent { get; set; }
        public string CreationTime { get; set; }
        public string LastModified { get; set; }
        public string StartDate { get; set; }
        public string RunForever { get; set; }
        public string Type { get; set; }
        public string Pacing { get; set; }
        public string DailyTarget { get; set; }
        public string MaximumAmount { get; set; }

    }
}
