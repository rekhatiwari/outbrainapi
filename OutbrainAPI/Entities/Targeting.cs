﻿using System.Collections.Generic;

namespace OutbrainAPI.Entities
{
    public class Targeting
    {
        public List<string> Platforms { get; set; }
    }
}
