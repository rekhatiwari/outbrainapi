﻿using System;

namespace OutbrainAPI
{
    public class TimeZoneHelper
    {
        public static DateTime ConvertToOutbrainTimezone(DateTime time)
        {
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(time, tz);
        }
    }
}
