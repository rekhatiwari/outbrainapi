﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutbrainAPI
{
    public class TokenSettingsHelper 
    {
        public static string TOKENCONFIGKEY = "Outbrain.AuthTokens";
        public static string TOKENSETTINGSFILE = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\OutbrainAuthtokens.json";
        Configuration config;

        private string Username;
        public TokenSettingsHelper(string username)
        {
            this.Username = username;
            this.config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        public string RetrieveAuthToken()
        {
            var allTokens = retrieveAllTokens();
            if (allTokens == null || allTokens.Count == 0)
                return null;

            var userToken = allTokens.Where(o => o.Username == this.Username).FirstOrDefault();
            if (userToken == null)
                return null;

            if (userToken.ExpiryDate <= DateTime.UtcNow.Date)
                return null;

            return userToken.Token;
        }

        public void StoreAuthToken(string newToken)
        {
            var allTokens = retrieveAllTokens();
            if (allTokens == null || allTokens.Count == 0)
                allTokens = new List<OutbrainToken>();

            //Remove the existing entry
            allTokens = allTokens.Where(o => o.Username != this.Username).ToList();

            allTokens.Add(new OutbrainToken()
            {
                Username = this.Username,
                ExpiryDate = DateTime.UtcNow.Date.AddDays(30),
                Token = newToken
            });


            storeAllTokens(allTokens);

        }

        private List<OutbrainToken> retrieveAllTokens()
        {
            using (var fs = new StreamReader(new FileStream(TOKENSETTINGSFILE, FileMode.OpenOrCreate)))
            {
                return JsonConvert.DeserializeObject<List<OutbrainToken>>(fs.ReadToEnd());
            }
        }

        private void storeAllTokens(List<OutbrainToken> tokens)
        {
            File.WriteAllText(TOKENSETTINGSFILE, JsonConvert.SerializeObject(tokens));
        }

        public class OutbrainToken
        {
            public string Username { get; set; }
            public DateTime ExpiryDate { get; set; }
            public string Token { get; set; }
        }
    }
}
