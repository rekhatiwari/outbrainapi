﻿using Elmah;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OutbrainAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            MainTaskAsync(args).Wait();
        }
        static async Task MainTaskAsync(String[] args)
        {
            try
            {
                DateTime from;
                DateTime to;
                from = DateTime.UtcNow.AddDays(-1);
                to = DateTime.UtcNow.AddDays(-1);
                bool isRealTimePull = false;
                //if (args.Length == 1)
                //{
                //    isRealTimePull = bool.Parse(args[0]);

                //    if (isRealTimePull)
                //    {
                //        // If real-time / intra-day pull, we default the dates
                //        from = DateTime.UtcNow;
                //        to = DateTime.UtcNow;
                //    }
                //    else
                //    {
                //        // Non real-time pulls default to past 3 days, this should be run once a day to refresh stats
                //        from = DateTime.UtcNow.AddDays(-1);
                //        to = DateTime.UtcNow.AddDays(-1);
                //    }
                //}
                //else if (args.Length == 2)
                //{
                //    // These arguments allow you to pull custom dates, should *not* be used for real-time pull, stats are way behind using this method [GetCampaignStats]
                //    isRealTimePull = false;

                //    from = DateTime.Parse(args[0]).ToUniversalTime();
                //    to = DateTime.Parse(args[1]).ToUniversalTime();
                //}
                //else
                //{
                //    Console.WriteLine("Invalid arguments");
                //    ConsoleExit();
                //    return;
                //}

                from = TimeZoneHelper.ConvertToOutbrainTimezone(from).Date;
                to = TimeZoneHelper.ConvertToOutbrainTimezone(to).Date;

                List<KeyValuePair<string, string>> logins = new List<KeyValuePair<string, string>>()
                {
                    //new KeyValuePair<string, string>( "scott@openmail.com", "Openmail1234" ),
                    //new KeyValuePair<string, string>( "billy@qool.com", "vdpp6ZDDvjr1" ),
                    new KeyValuePair<string, string>( "todaysinfo", "Riverrich1" ),
                    //new KeyValuePair<string, string>( "qoolmedia", "W4y2qool" )
                };


                foreach(var login in logins)
                {
                    await RunAsync(from, to, isRealTimePull, login.Key, login.Value);
                }

                ConsoleExit();

            }
            catch (Exception e)
            {
                ErrorLog.GetDefault(null).Log(new Error(e));
            }
        }


        static async Task RunAsync(DateTime from, DateTime to, bool isRealTimePull, string username, string password)
        {
            Console.WriteLine("{0} | {1}", username, password);
            
            var obHelper = new OutbrainHelper(isRealTimePull);
            await obHelper.SetToken(username, password);
            await obHelper.GetMarketers();
            
            await obHelper.GetCampaigns((DateTime.UtcNow.Date - from).Days > 7);    //Include the archive on polls greater than a week ago.
            await obHelper.GetCampaignURLs();
            await obHelper.GetCampaignStats(from, to);
            await obHelper.GetPerformanceDetails(from, to);

            await obHelper.LoadCampaignStats(from, to);
            
            Console.WriteLine("Finished {0} | {1}", username, password);
            //await obHelper.ExportCampaignStats();
        }

        static void ConsoleExit()
        {
#if DEBUG
            Console.WriteLine("Press <any> key to exit ...");
            Console.ReadKey();
#endif
        }

    }
}
