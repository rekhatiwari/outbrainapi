﻿using OutbrainAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using Elmah;

namespace OutbrainAPI
{
    public class Db
    {
        private static Dictionary<string, SpendAccount> spendAccountLookup;

        static Db()
        {
            spendAccountLookup = new Dictionary<string, SpendAccount>();
        }

        public static List<SpendReport> LoadSpendReportData(bool isRealTimePull, List<Marketers> marketers, DateTime fromDate, DateTime toDate)
        {
            
            List<string> missingSpendAccounts = new List<string>();

            List<SpendReport> srData = new List<SpendReport>();
            foreach (var marketer in marketers)
            {
                foreach (var campaign in marketer.Campaigns)
                {
                    try
                    {
                        if (campaign.PerformanceReport.OverallMetrics.Cost.ParseAs<decimal>(0) == 0)
                            continue;
                        if (campaign.PromotedLinks.Count == 0)
                            continue;
                        // Parse domain from url
                        var activeCampaigns = campaign.PromotedLinks.Where(o => o.PromotedLinkReport != null && o.PromotedLinkReport.OverallMetrics.Cost != "0").ToList();
                        var url = activeCampaigns.First().Url;
                        string domain = HttpHelper.RootDomain(url);

                        //Attempt to parse the domain from the imp parameter
                        if (!String.IsNullOrEmpty(new UriBuilder(url).Query))
                        {
                            try
                            {
                                var qs = new UriBuilder(url).Query;
                                var imp = HttpUtility.ParseQueryString(qs).AllKeys.Where(o => o == "imp").FirstOrDefault();
                                if (!String.IsNullOrEmpty(imp)){
                                    domain = HttpUtility.ParseQueryString(qs)["imp"];
                                    if (domain.Contains(","))
                                        domain = domain.Split(',')[0];
                                }
                            }catch(Exception e)
                            {
                                //Ignore, use the domain value from the url
                            }
                            
                        }
                        // Concat marketer/account name with domain for SpendAccount.Username lookup
                        var username = string.Format("{0}_{1}", marketer.Name, domain);
                        var sa = Db.GetSpendAccount(username);

                        if (sa == null)
                        {
                            // Need to send out notification email notifying that the Spend Account cannot be found in the DB, and that it must be setup.  Email to be sent at end of function call.
                            if (!missingSpendAccounts.Contains(username))
                            {
                                missingSpendAccounts.Add(username);
                            }
                            continue;
                        }

                        if (isRealTimePull)
                        {
                            var sr = new SpendReport()
                            {
                                Date = TimeZoneHelper.ConvertToOutbrainTimezone(DateTime.UtcNow).Date,
                                AdwordsAccount = sa.Username,
                                AdwordsId = sa.Id,
                                Source = sa.SpendProvider.ProviderName,
                                Campaign = campaign.Name,
                                Cost = campaign.LiveStatus.AmountSpent.ParseAs<decimal>(0),
                                SiteId = sa.SiteId,
                                DestinationDomain = domain
                            };
                            srData.Add(sr);
                        }
                        else
                        {
                            foreach (var pl in campaign.PromotedLinks.Where(o=>o.PromotedLinkReport != null))
                            {
                                foreach (var stat in pl.PromotedLinkReport.details)
                                {
                                    string adgroup = "Unknown";
                                    try
                                    {
                                        adgroup = Regex.Match(pl.Url, "utm_content=([0-9]+)").Groups[1].Value;
                                    }
                                    catch (Exception e)
                                    {
                                        adgroup = "Unknown";
                                        //Ignore
                                    }
                                    var sr = new SpendReport()
                                    {
                                        Date = stat.Date.ParseAs<DateTime>(),
                                        AdwordsAccount = sa.Username,
                                        AdwordsId = sa.Id,
                                        Source = sa.SpendProvider.ProviderName,
                                        Campaign = campaign.Name,
                                        Clicks = stat.Clicks.ParseAs<int>(),
                                        Impressions = stat.Impressions.ParseAs<int>(),
                                        Cost = stat.Cost.ParseAs<decimal>(),
                                        SiteId = sa.SiteId,
                                        DestinationDomain = domain,
                                        AdGroup = adgroup,
                                    };

                                    //Do a date check, Sometimes weird dates slip in which screws up deletion and insertion
                                    if (sr.Date < fromDate || sr.Date > toDate)
                                        continue;
                                    if (sr.Impressions == 0 && sr.Cost == new decimal(0) && sr.Clicks == 0)
                                        continue;

                                    srData.Add(sr);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Console.WriteLine("Something went wrong when building the spendreport data" + e.Message);
                    }
                }
            }

          if (srData.Count > 0)
            {
                var minDate = srData.Select(s => s.Date).Min();
                var maxDate = srData.Select(s => s.Date).Max();

                using (var data = new RSTEntities())
                {
                    // Out with the old
                    Console.WriteLine("Deleting old data fom the db ...");
                    foreach (var id in srData.Select(s => s.AdwordsId).Distinct())
                    {
                        data.Database.ExecuteSqlCommand("DELETE FROM dbo.SpendReports WHERE AdwordsId = @p0 AND Date >= @p1 AND Date <= @p2", id.Value, minDate.ToString("yyyy-MM-dd"), maxDate.ToString("yyyy-MM-dd"));
                    }

                    // In with the new
                    Console.WriteLine("Loading new data to the db ...");
                    foreach (var sr in srData)
                    {
                        data.SpendReports.Add(sr);
                    }
                    data.SaveChanges();
                }

                // Load SCM_SPS data for date range & account
                using (var data = new RSTEntities())
                {
                    Console.WriteLine("Update SCM_SPS for each account ...");
                    foreach (var id in srData.Select(s => s.AdwordsId).Distinct())
                    {
                        data.SCM_SPS_SpendReports_Single_Populate(minDate, maxDate, id.Value);
                    }
                }

                //Push to SPSv2
                //using (var data = new SPSEntities())
                //{
                //    Console.WriteLine("Update SPSv2 for each account ...");
                //    foreach (var id in srData.Select(s => s.AdwordsId).Distinct())
                //    {
                //        try
                //        {
                //            data.SPS_SpendStats_Populate(minDate, maxDate, id.Value);
                //        }catch(Exception e)
                //        {
                //            ErrorLog.GetDefault(null).Log(new Error(e));
                //        }
                //    }
                //}
            }

            // now send out missing spend accounts email
            if (missingSpendAccounts.Count > 0)
            {
                string missingSpendAccountsString = "";

                foreach (var spendAccount in missingSpendAccounts)
                {
                    missingSpendAccountsString += spendAccount + Environment.NewLine;
                }

                EmailHelper.Send(ConfigurationManager.AppSettings["EmailSendTo"] ?? "support@p4pi.net",
                                            "Outbrain - Missing Spend Accounts",
                                            "The following Spend Accounts cannot be found by Outbrain.  "
                                            + "Please setup the 'username' field with the id(s) listed below - in order for Outbrain to recognize these Spend Accounts:"
                                            + Environment.NewLine + Environment.NewLine + missingSpendAccountsString);
            }

            return srData;
        }
        
        public static SpendAccount GetSpendAccount(string id)
        {
            SpendAccount account;
            if (spendAccountLookup.TryGetValue(id, out account))
                return account;
            else
            {
                using (var data = new RSTEntities())
                {
                    account = data.SpendAccounts.Include("SpendProvider").Where(s => s.Username == id).FirstOrDefault();
                }
                spendAccountLookup.Add(id, account);
                return account;
            }
        }
    }
}
