﻿using System;
using System.Net.Mail;

namespace OutbrainAPI
{
    public class EmailHelper
    {
        public static void Send(string toAddress, string subject, string body)
        {
            MailMessage message = new MailMessage("no-reply@p4pi.net", toAddress, subject, body);
            using (SmtpClient client = new SmtpClient())
            {
                client.Timeout = 30000;  // 30 seconds
                client.Send(message);
            }
        }

    }
}
