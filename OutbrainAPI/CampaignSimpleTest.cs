﻿namespace OutbrainAPI
{
    public class CampaignSimpleTest
    {

        public string Date { get; set; }
        public string Marketer { get; set; }
        public int SpendAccountId { get; set; }
        public string Campaign { get; set; }
        public string PromotedLinkFirst { get; set; }
        public string Impressions { get; set; }
        public string Clicks { get; set; }
        public string Cost { get; set; }

    }
}
